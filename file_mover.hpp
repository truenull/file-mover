#ifndef FILE_MOVER_HPP
#define FILE_MOVER_HPP

#include <QObject>
#include <QDebug>
#include <QFile>

class file_mover : public QObject
{
    Q_OBJECT
public:
    explicit file_mover(QObject *parent = 0);

private:

public slots:
    void run();

signals:
    void finished();
};

#endif // FILE_MOVER_HPP
