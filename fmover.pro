APPNAME = FileMover
WINDOWS = true

QT       += core

QT       -= gui
TARGET = fmover
CONFIG   += console
CONFIG   -= app_bundle

QMAKE_PROJECT_NAME = APPNAME

TEMPLATE = app

SOURCES += main.cpp \
    file_mover.cpp

HEADERS += \
    file_mover.hpp \
    settings.hpp

DESTDIR = $${OUT_PWD}/dist

# Copy necessary qt binaries in Windows builds
# the trailing slash is important...
win32 {

qtlibs.path = $${DESTDIR}/
message(QtLibspath is $${qtlibs.path})
message($$[QT_INSTALL_LIBS])
qtlibs.files += $$[QT_INSTALL_LIBS]/../bin/icuin51.dll
qtlibs.files += $$[QT_INSTALL_LIBS]/../bin/icuuc51.dll
qtlibs.files += $$[QT_INSTALL_LIBS]/../bin/icudt51.dll

message(The project will be installed in $$DESTDIR)

CONFIG(release, debug|release) {
qtlibs.files += $$[QT_INSTALL_LIBS]/../bin/Qt5Core.dll
} else {
qtlibs.files += $$[QT_INSTALL_LIBS]/../bin/Qt5Cored.dll
}

INSTALLS += qtlibs
} # End of Windows specific stuff

# This is a C++11 project, g++ needs to be told.
linux-g++ {
    QMAKE_CXXFLAGS += -std=c++11
}

# DEFINES
DEFINES += APP_NAME=\\\"$${APPNAME}\\\"
DEFINES += ORG_NAME=\\\"TrueNull.se\\\"
