FileMover
=========

FileMover moves files with different file endings to
configurable directories. This is can be used to sort 
files by file-endings if you for example set the binary
as the open handler for a particular file type in your 
web browser.

Usage
-----
    fmover -o /path/to/move/to [-t type]

After that running: 

    fmover /full/path/to/file.type 

Will move the file to the directory specified by the -o 
parameter in the first command. Note that the -t switch is
optional and will apply to files without a file-ending.

Compiling
---------
The only dependency for FileMover is version 5.2.1 of Qt.
You can get it from Qt-project.org or your favourite package
manager.

You can grab the source from the [gitrepository] using git or 
as an archive from the Branches tab of the Downloads page.

[gitrepository]:https://bitbucket.org/truenull/file-mover

### Linux
We're going to assume that you have G++, Qt5 and GNU Make already installed.

    git clone git@bitbucket.org:truenull/file-mover.git
    mkdir build && cd build
    qmake ../file-mover
    make


### Windows
Open the .pro file with QtCreator (that came with Qt).

This should be enough to get a build enabled. If you want to
distribute binaries you either have to add a buildstep with a
"install" argument to jom (which will copy the needed binaries) 
or tell users to install Qt5 and add it to their  PATH.

Please note that the first option places LGPL compliance 
requirements on you.

You could do the Linux steps above (replacing make with jom)
 but that is more complicated because you have to set up more
 command-line variables and such to get it to work.

License
-------
This software is licensed under a 3-Clause BSD license.
This only applies to the source code in this particular 
project. In binary distributions with dependencies
(Qt5 & ICU) you will need to be able to satisfy their license
requirements as well.


