#ifndef SETTINGS_HPP
#define SETTINGS_HPP
#include <QDebug>
#include <QString>
#include <QFile>

const QString OUTPUT_PATH_KEY = "OUTPUT_PATH";

QFile logfile;

QString get_output_path_key(QString filetype)
{
    QString key(OUTPUT_PATH_KEY);

    if(filetype.length () != 0)
    {
        key = key.append(QString("-"))
              .append(filetype.toLower());
    }

    return key;
}

#endif // SETTINGS_HPP
