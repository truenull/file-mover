#include <QCoreApplication>
#include <QtMessageHandler>
#include <QTimer>
#include <QStringList>
#include <QSettings>
#include <QDebug>
#include <QFileInfo>
#include <QDir>
#include "settings.hpp"
#include "file_mover.hpp"
#include <iostream>

const int QINDEX_NOT_FOUND = -1;

#ifdef DEBUG
void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    //in this function, you can write the message to any stream!
     QTextStream outstream(&logfile);

    switch (type) {
    case QtDebugMsg:
        outstream<<QString("Debug(%2): %1\n").arg(msg, QString::number(context.line));
        break;
    case QtWarningMsg:
        outstream<<QString("Warning(%2): %1\n").arg(msg, QString::number(context.line));
        break;
    case QtCriticalMsg:
        outstream<<QString("Critical(%2): %1\n").arg(msg, QString::number(context.line));
        break;
    case QtFatalMsg:
        outstream<<QString("Fatal(%2): %1\n").arg(msg, QString::number(context.line));
        abort();
    }
}
#endif

int set_type_output(QCoreApplication&, QString, QString);
void print_usage(QString binary);

int main(int argc, char *argv[])
{
    logfile.setFileName(QDir::homePath().append(QString("/debug.log")));
    logfile.open(QIODevice::WriteOnly);

    //qInstallMessageHandler(myMessageOutput);


    QCoreApplication app(argc, argv);

    QFileInfo finfo(argv[0]);

    app.setApplicationName(QString(APP_NAME));
    app.setOrganizationName(QString(ORG_NAME));

    QStringList sl = app.arguments();

    if(sl.count () == 1
            || sl.contains(QString("-h"))
            || sl.contains(QString("--help")))
    {
        print_usage(sl.first ());
        return EXIT_SUCCESS;
    }

    if(sl.contains(QString("-o")))
    {
        if(sl.count () < 3)
        {
            qDebug()<<"Output location needs a second argument.";
            return EXIT_FAILURE;
        }
        else if(sl.count () < 5 && sl.count() != 3)
        {
            qDebug()<<"Both Output location and Type needs one argument.";
            
            return EXIT_FAILURE;
        }
        else if (sl.count() > 5)
        {
            qDebug() << "Too many arguments with arguments -o present";

        }

        int output_position = sl.indexOf("-o") + 1;
        int type_position = sl.indexOf ("-t") + 1;
        QString output;

        // Verify indexes
        if(output_position!=QINDEX_NOT_FOUND)
        {
            output = sl[output_position];
        }
        else
        {
            // Should never happen...
            qDebug() << "-o argument not found after being in argument list.";
            return EXIT_FAILURE;
        }

        // Type is optional
        QString type = (type_position == QINDEX_NOT_FOUND)? "" : sl[type_position];
        return set_type_output(app, output, type);
    }

    file_mover *mover = new file_mover(&app);

    QObject::connect(
                mover,
                SIGNAL(finished()),
                &app,
                SLOT(quit())
                );
    QTimer::singleShot(0, mover, SLOT(run()));


    int res = app.exec();


    logfile.close();
    return res;
}

int set_type_output(QCoreApplication &app, QString output, QString type)
{
    // Store new output location
    QSettings pref(&app);
    QString key = get_output_path_key(type);
    pref.setValue(key, output);
    return EXIT_SUCCESS;
}

void print_usage(QString binary)
{
    std::string bin = binary.toStdString ();
    std::cout << bin << " moves files run with the program. Configure directory like this:" << std::endl;
    std::cout << bin << " -o /path/to/move/to [-t type]" << std::endl;

    std::cout << "After that running:" << std::endl;
    std::cout << bin << " /full/path/to/file.type" << std::endl;
    std::cout << "Will move the file to the specified directory."
              << "This is can be useful if you for example set the binary"
              <<" as the open handler for a particular file type in your "
              << "web browser." << std::endl;
    std::cout << "Uses the Qt library under the GNU Lesser General Public License version 2.1" << std::endl;
}

