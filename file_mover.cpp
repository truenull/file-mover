#include <QCoreApplication>
#include <QFileInfo>
#include <QDir>
#include <QFile>
#include <QSettings>

#include "file_mover.hpp"
#include "main.cpp"
#include "settings.hpp"

file_mover::file_mover(QObject *parent) :
     QObject(parent)
{
}

void file_mover::run()
{
    QCoreApplication * app = (QCoreApplication*)this->parent();
    QSettings prefs(app);
    QDir dir(QDir::currentPath());
    QString path;

    for(QString arg : app->arguments())
    {
        QFileInfo file = QFileInfo(arg);

        // Get the output path for this filetype
        path = prefs.value(get_output_path_key(file.suffix()), "")
               .toString();

        // Maybe it's a filetype with multiple suffixes like tar.gz?
        if(path.isEmpty ())
        {
            path = prefs.value(get_output_path_key(file.completeSuffix()), "")
                   .toString();
        }

        if(path.isEmpty())
        {
            // No output defined for this filetype, on to next one.
            continue;
        }

        QString output = path;
        if(app->applicationName() != file.fileName() && file.exists())
        {
           qDebug() << arg << path
                   << "Result:"
                 << dir.rename(arg, output.append("/").append(file.fileName()));
        }
    }

    emit finished();
}
